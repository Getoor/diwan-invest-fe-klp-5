import React from "react";
import Home from "../../assets/img/home.png";
import Typed from "react-typed";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const ContentDashboard = () => {
  const { user } = useSelector((store) => store.user);

  return (
    <>
      <div className="w-full bg-white py-16 px-4">
        <div className="max-w-[1240px]  mx-auto grid md:grid-cols-2">
          <img className="w-[700px] h-[700px] mx-auto my-4" src={Home} alt="/" />
          <div className="flex flex-col justify-center">
            <h1 className="md:text-2xl sm:text-3xl text-2xl font-bold  font-[poppins]">{`Halo, ${user?.name}`}</h1>
            <h1 className="md:text-4xl sm:text-3xl text-2xl font-bold font-[poppins]">invest your money without fear</h1>
            <Typed className="font-bold text-2xl font-[poppins]" strings={["Konservatif", "Moderat", "Agresif"]} typeSpeed={120} backSpeed={140} loop />

            <div className="grid grid-cols-2">
              {user?.score_quistioner == null ? (
                <Link to={"/question"}>
                  <button className="bg-[#FDDA1C] text-black font-semibold w-[200px] rounded-md  my-6  md:mx-0 py-3">Get Now Quisioner</button>
                </Link>
              ) : (
                <div>
                  <p className="bg-[#FDDA1C] text-black font-semibold w-[250px] rounded-md p-3  my-6 mx-auto md:mx-0 py-3">Anda telah mengisi quisioner</p>
                </div>
              )}

              <Link to={"/konsultan"}>
                <button className="bg-[#FDDA1C] text-black font-semibold w-[200px] rounded-md  my-6 mx-auto md:mx-0 py-3">See Consultan</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContentDashboard;
