import React from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import { MdDashboard } from "react-icons/md";
import { RiUserSmileFill } from "react-icons/ri";
import confirm from "antd/lib/modal/confirm";
import { ExclamationCircleFilled } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { logoutRm } from "../../utils/rm/ConsultanSlice";

const LeftBar = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const showConfirm = () => {
    confirm({
      title: "Apa anda ingin keluar ? ",
      icon: <ExclamationCircleFilled />,
      onOk() {
        dispatch(logoutRm());
        navigate("/rm-login");
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };
  return (
    <div className="w-screen min-h-screen flex">
      <aside className="w-[300px] h-screen flex flex-col bg-white shadow-lg">
        <div className="w-full h-[300px] flex flex-col justify-center items-center bg-gradient-to-tr bg-yellow-300 ">
          <h1 className="text-2xl text-slate-700 font-[poppins] font-extrabold">#DIWAN INVEST RM</h1>
          <div className="w-100 h-5 bg-black"></div>
        </div>
        <nav className="w-full flex-1 bg-yellow-200">
          <Link to={"/rm"} className="w-full h-12 flex items-center px-4 text-lg text-slate-700 font-bold hover:bg-slate-100 hover:text-slate-700 font-[poppins]">
            <MdDashboard /> Dashboard
          </Link>
          <Link to={"consumers"} className="w-full h-12 flex items-center px-4 text-lg text-slate-700 font-bold hover:bg-slate-100 hover:text-slate-700 font-[poppins]">
            <RiUserSmileFill /> Consumer
          </Link>
          <p onClick={showConfirm} className="cursor-pointer mt-5 w-full h-12 flex items-center px-4 text-lg text-slate-700 font-bold hover:bg-slate-100 hover:text-slate-700 font-[poppins]">
            Logout
          </p>
        </nav>
      </aside>

      <div className="flex-1 h-screen  overflow-y-auto">
        <Outlet />
      </div>
    </div>
  );
};

export default LeftBar;
