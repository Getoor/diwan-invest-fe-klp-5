import React from "react";

const NavRm = (props) => {
  return (
    <div>
      <nav className="  text-center py-4 px-6 bg-white shadow  w-full">
        <div className="mb-2 sm:mb-0">
          <div className="text-2xl font-semibold font-[poppins] text-right ">Hallo {props.name} </div>
        </div>
      </nav>
    </div>
  );
};

export default NavRm;
