import React, { useState } from "react";
import { GrClose } from "react-icons/gr";
import { VscAccount } from "react-icons/vsc";
import { FiLogOut } from "react-icons/fi";
import { GiHamburgerMenu } from "react-icons/gi";
import { ExclamationCircleFilled } from "@ant-design/icons";
import { Modal } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logoutUser } from "../../utils/user/UserSlice";

const NavbarDashboard = () => {
  let [open, setOpen] = useState(false);
  const { confirm } = Modal;

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const showConfirm = () => {
    confirm({
      title: "Apa anda ingin keluar ? ",
      icon: <ExclamationCircleFilled />,
      onOk() {
        dispatch(logoutUser());
        navigate("/");
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  return (
    <div className="shadow-md w-full fixed top-0 left-0">
      <div className="md:flex items-center justify-between max-w-[1240px] mx-auto h-24 bg-white py-4 md:px-10 px-7">
        <div className="font-bold text-2xl cursor-pointer flex items-center font-[poppins] text-gray-800">Diwan Invest</div>
        <div onClick={() => setOpen(!open)} className="text-3x1 absolute right-10 top-8 cursor-pointer md:hidden">
          {open ? <GrClose /> : <GiHamburgerMenu />}
        </div>
        <ul className={`md:flex md:items-center font-[poppins] md:pb-0 pb-12 absolute md:static bg:white md:z-auto z-[-1] left-0 w-full md:w-auto md:pl-0 pl-9 transition-all duration-500 ease-in ${open ? "top-20" : "top-[-490px]"}`}>
          <li className="md:ml-8 text-lg md:my-0 my-7">
            <a href="/home" className="text-black">
              Home
            </a>
          </li>
          <li className="md:ml-8 text-lg md:my-0 my-7">
            <a href="#category" className="text-black">
              Kategori
            </a>
          </li>
          <li className="md:ml-8 text-lg md:my-0 my-7">
            <a href="#contact" className="text-black">
              Kontak
            </a>
          </li>
          <li className="md:ml-8 text-lg md:my-0 my-7">
            <NavLink to={"/profil-page"}>
              <VscAccount color="#FDDA1C" size="30px" className="cursor-pointer" />
            </NavLink>
          </li>
          <li className="md:ml-8 text-lg md:my-0 my-7">
            <FiLogOut color="#FDDA1C" size="30px" onClick={showConfirm} className="cursor-pointer" />
          </li>
        </ul>
      </div>
    </div>
  );
};

export default NavbarDashboard;
