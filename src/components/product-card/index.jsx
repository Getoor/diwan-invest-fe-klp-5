import React from "react";

const ProductCard = (props) => {
  return (
    <div>
      <div className="rounded-xl shadow-lg bg-white">
        <div className="p-5 flex flex-col">
          <div className="rounded-full overflow-hidden mx-auto  w-36 h-36 bg-yellow-200 ">
            <p className="text-slate-700 font-[poppins] mt-10 text-center text-6xl font-extrabold">{props.firstletter}</p>
          </div>
          <p className="text-xl font-medium mt-1">{props.productname}</p>
          <p className=" font-medium ">{props.product}</p>
          <p className="text-slate-600 text-justify ">{props.desc}</p>
          <a href={props.url} target={"_blank"} className="text-center bg-yellow-400 py-2 rounded-lg text-black hover:bg-yellow-500 font-semibold">
            Beli
          </a>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
