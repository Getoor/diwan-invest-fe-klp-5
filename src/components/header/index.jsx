import React, { useState } from "react";
import { GrClose } from "react-icons/gr";
import { GiHamburgerMenu } from "react-icons/gi";
import { NavLink } from "react-router-dom";

const Header = () => {
  let [open, setOpen] = useState(false);
  return (
    <>
      <div class="border-t-8 border-yellow-400"></div>
      <div class="border-b-2 border-yellow-400"></div>
      <div className="md:flex items-center justify-between  mx-auto h-24  py-4 md:px-10 px-7">
        <NavLink to={"/"}>
          <div className="ml-10 font-extrabold text-2xl cursor-pointer flex items-center font-[poppins] text-yellow-400">
            #DiwanInvest
          </div>
        </NavLink>
        <div
          onClick={() => setOpen(!open)}
          className="text-3x1 absolute right-10 top-8 cursor-pointer md:hidden"
        >
          {open ? <GrClose /> : <GiHamburgerMenu />}
        </div>
        <ul
          className={`mt-auto mr-10 md:flex md:items-center font-[poppins] md:pb-0 pb-12 absolute md:static bg:white md:z-auto z-[-1] left-0 w-full md:w-auto md:pl-0 pl-9 transition-all duration-500 ease-in ${
            open ? "top-20 ml-10" : "top-[-490px]"
          }`}
        >
          <li className="md:ml-8 text-lg md:my-0 my-7">
            <NavLink to={"/product"} className="text-slate-800 font-medium">
              Product
            </NavLink>
          </li>
          <li className="md:ml-8 text-lg md:my-0 my-7">
            <NavLink to={"/feature"} className="text-slate-800 font-medium">
              Feature
            </NavLink>
          </li>
          <li className="md:ml-8 text-lg md:my-0 my-7">
            <NavLink to={"/about"} className="text-slate-800 font-medium">
              About
            </NavLink>
          </li>
          <li className="md:ml-8 text-base md:my-0 my-7">
            <NavLink to={"/login"}>
              <button class="bg-yellow-400 hover:bg-yellow-500 text-black w-32 font-medium py-2 px-4 rounded-md">
                Login
              </button>
            </NavLink>
          </li>
          <li className="md:ml-8 text-base md:my-0 my-7">
            <NavLink to={"/register"}>
              <button class="bg-yellow-400 hover:bg-yellow-500 text-black w-32 font-medium py-2 px-4 rounded-md">
                Register
              </button>
            </NavLink>
          </li>
        </ul>
      </div>
    </>
  );
};

export default Header