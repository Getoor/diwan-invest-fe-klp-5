import React from "react";
import Agresif from "../../assets/img/agresif.png";
import Konservatif from "../../assets/img/konservatif.png";
import Moderat from "../../assets/img/moderat.png";

const CategoryDashboard = () => {
  return (
    <div id="category" className="py-28 px-4 mb-32">
      <h1 className="text-center text-2xl font-[poppins] font-bold"> KATEGORI </h1>
      <div className="inline-flex justify-center items-center w-full">
        <hr className="my-8 w-64 h-1 bg-gray-200 rounded border-0 dark:bg-gray-700" />
        <div className="absolute left-1/2 px-4 bg-white -translate-x-1/2 dark:bg-gray-900"></div>
      </div>
      <div className="w-full py-[2rem] px-4 bg-white">
        <div className="max-w-[1240px] mx-auto grid md:grid-cols-3 gap-8">
          <div className="w-full shadow-xl flex flex-col p-4 my-4 rounded-lg">
            <img className="" src={Konservatif} alt="/" />
            <button className="bg-[#FDDA1C] w-[200px] rounded-md font-medium my-6 mx-auto px-6 py-3">KONSERVATIF</button>
          </div>
          <div className="w-full shadow-xl flex flex-col p-4 my-4 rounded-lg">
            <img className="" src={Moderat} alt="/" />
            <button className="bg-[#FDDA1C] w-[200px] rounded-md font-medium my-6 mx-auto px-6 py-3">MODERAT</button>
          </div>
          <div className="w-full shadow-xl flex flex-col p-4 my-4 rounded-lg">
            <img className="" src={Agresif} alt="/" />
            <button className="bg-[#FDDA1C] w-[200px] rounded-md font-medium my-6 mx-auto px-6 py-3">AGRESIF</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CategoryDashboard;
