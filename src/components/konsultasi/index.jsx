import React, { useEffect, useState } from "react";
import api from "../../service/api";
import { Rate } from "antd";
import { useSelector } from "react-redux";

const Konsultasi = () => {
  const [consultan, setConsultan] = useState([]);
  const { user } = useSelector((store) => store.user);

  const fetchConsultant = async () => {
    try {
      const url = "/api/v1/consultant";
      const response = await api.get(url);
      const payload = [...response.data];
      setConsultan(payload);
    } catch (error) {
      //toast.error(error.response.data.data);
      console.log(error);
    }
  };

  useEffect(() => {
    fetchConsultant();
  }, []);

  return (
    <>
      <div className="mx-6 py-10">
        <h1 className="text-center text-2xl font-[poppins] font-bold"> Para Konsultan Diwan Invest </h1>
        <div className="inline-flex justify-center items-center w-full">
          <hr className="my-2 w-64 h-1 bg-gray-200 rounded border-0 dark:bg-gray-700" />
          <div className="absolute left-1/2 px-4 bg-white -translate-x-1/2 dark:bg-gray-900"></div>
        </div>

        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 mt-5">
          {consultan.map((items) => {
            return (
              <div key={items.consultantid} className="shadow-lg hover:bg-slate-300 rounded-lg bg-slate-200 m-3">
                <div className="p-2 flex flex-col">
                  <div className="rounded-full mx-auto overflow-hidden w-36 h-36 bg-yellow-300 ">
                    <p className="text-slate-700 font-[poppins] mt-10 text-center text-6xl font-extrabold">{items.name.charAt(0)}</p>
                  </div>
                  <div className="mx-auto mt-2">
                    <Rate className="h-5 " defaultValue={items.rate} />
                  </div>
                  <p className="text-2xl text-center font-semibold mt-2">{items.name}</p>
                  <p className="text-xl text-center font-medium ">Experience Selama {items.experience}</p>
                  <p className="text-slate-600 text-lg text-justify">{items.description.substring(0, 100)} ....</p>

                  <div className=" m-auto ">
                    <div className="dark:bg-slate-600 w-40 p-3 ml-3 text-center text-white font-medium text-base rounded-lg hover:bg-slate-700 cursor-pointer shadow-lg">{items?.users?.length} Consumer</div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Konsultasi;
