import { configureStore } from "@reduxjs/toolkit";
import ConsultanSlice from "./utils/rm/ConsultanSlice";
import userSlice from "./utils/user/UserSlice";

export const store = configureStore({
  reducer: {
    user: userSlice,
    rm: ConsultanSlice,
  },
});
