// save user data to local storage
export const addUserToLocalStorage = (user) => {
  localStorage.setItem("user", JSON.stringify(user));
};

export const removeUserFromLocalStorage = () => {
  localStorage.removeItem("user");
};

export const getUserFromLocalStorage = () => {
  const result = localStorage.getItem("user");
  const user = result ? JSON.parse(result) : null;
  return user;
};

// save rm data to local storage
export const addRmToLocalStorage = (rm) => {
  localStorage.setItem("rm", JSON.stringify(rm));
};

export const removeRmFromLocalStorage = () => {
  localStorage.removeItem("rm");
};

export const getRmFromLocalStorage = () => {
  const result = localStorage.getItem("rm");
  const user = result ? JSON.parse(result) : null;
  return user;
};
