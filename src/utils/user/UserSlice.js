import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import api from "../../service/api";
import { addUserToLocalStorage, getUserFromLocalStorage, removeUserFromLocalStorage } from "../storage/localStrorage";

export const sessionToken = () => {
  JSON.parse(localStorage.getItem("token"));
};

const initialState = {
  isLoading: false,
  user: getUserFromLocalStorage(),
};

export const registerUser = createAsyncThunk("user/registerUser", async (user, thunkAPI) => {
  try {
    const resp = await api.post("/api/v1/signup", user);
    toast.success("User Saved.....");
    if (resp.data.status === 200) {
      setTimeout(() => {
        window.location.href = "http://localhost:3000/login";
      }, 2000);
    }
    return resp.data;
  } catch (error) {
    toast.error(error.response.data.data);
    return thunkAPI.rejectWithValue(error.response.data.data);
  }
});

export const loginUser = createAsyncThunk("user/loginUser", async (user, thunkAPI) => {
  try {
    const resp = await api.post("/api/v1/signin", user);
    //console.log(resp.data.data.user);
    setTimeout(() => {
      window.location.href = "http://localhost:3000/home";
    }, 1000);
    return resp.data;
  } catch (error) {
    toast.error(error.response.data.data);
    return thunkAPI.rejectWithValue(error.response.data.data);
  }
});

export const putScoreUser = createAsyncThunk("user/putScoreUser", async (user, thunkAPI) => {
  try {
    const resp = await api.put("/api/v1/setscoreandconsult", user);
    if (resp.data.status === 200) {
      toast.success(`Hello ${resp.data.data.name}, Silahkan tunggu konsultan menghubungi anda`);
      removeUserFromLocalStorage();
      toast.dark("silahkan login ulang yah...");
      setTimeout(() => {
        window.location.href = "http://localhost:3000/";
      }, 1000);
    }
    return resp.data;
  } catch (error) {
    toast.error(error.response.data.data);
    return thunkAPI.rejectWithValue(error.response.data);
  }
});

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    logoutUser: (state) => {
      state.user = null;
      removeUserFromLocalStorage();
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(registerUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(registerUser.fulfilled, (state, { payload }) => {
        const { data } = payload;
        state.isLoading = false;
        state.data = data;
        toast.success(`Terima kasih kakak ${data?.name}  sudah daftar `);
      })
      .addCase(registerUser.rejected, (state, { payload }) => {
        state.isLoading = false;
      })
      .addCase(loginUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(loginUser.fulfilled, (state, { payload }) => {
        const { data } = payload;
        state.isLoading = false;
        state.data = data;
        addUserToLocalStorage(data);
        toast.dark(`Selamat Datang Kakak ${data?.name}`);
      })
      .addCase(loginUser.rejected, (state, { payload }) => {
        state.isLoading = false;
      })
      .addCase(putScoreUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(putScoreUser.fulfilled, (state, { payload }) => {
        state.isLoading = false;
      })
      .addCase(putScoreUser.rejected, (state, { payload }) => {
        state.isLoading = false;
        toast.error(payload);
      });
  },
});

export const { logoutUser } = userSlice.actions;
export default userSlice.reducer;
