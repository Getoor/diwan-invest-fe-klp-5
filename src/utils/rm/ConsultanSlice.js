import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import api from "../../service/api";
import { addRmToLocalStorage, getRmFromLocalStorage, removeRmFromLocalStorage } from "../storage/localStrorage";

const initialState = {
  isLoading: false,
  rm: getRmFromLocalStorage(),
};

export const loginRm = createAsyncThunk("rm/loginRm", async (rm, thunkApi) => {
  try {
    const resp = await api.post("/api/v1/signinconsultant", rm);
    if (resp.data.status === 200) {
      setTimeout(() => {
        window.location.href = "http://localhost:3000/rm";
      }, 2000);
    }
    return resp.data;
  } catch (error) {
    toast.error(error.response.data.data);
    //console.log(error.response.data.data);
    return thunkApi.rejectWithValue(error.response.data.data);
  }
});

const consultantSlice = createSlice({
  name: "rm",
  initialState,
  reducers: {
    logoutRm: (state) => {
      state.rm = null;
      removeRmFromLocalStorage();
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginRm.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(loginRm.fulfilled, (state, { payload }) => {
        const { data } = payload;
        state.isLoading = false;
        state.data = data;
        addRmToLocalStorage(data);
        toast.dark(`Selamat Datang RM ${data.name}`);
      })
      .addCase(loginRm.rejected, (state, { payload }) => {
        state.isLoading = false;
      });
  },
});

export const { logoutRm } = consultantSlice.actions;
export default consultantSlice.reducer;
