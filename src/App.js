import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { LandingPage, Home, Login, Register, About, Feature, Product, Konsultan, Quiz, LoginRm, DashboardRm, RmDashboard, DetailConsultant, ProductRecom, FormConsul, ProfilPage, ErrorPage, ConsumerRm, RmListProduct } from "./pages";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/home" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/feature" element={<Feature />} />
        <Route path="/product" element={<Product />} />
        <Route path="/*" element={<ErrorPage />} />
        <Route path="/konsultan" element={<Konsultan />} />
        <Route path="/question" element={<Quiz />} />
        <Route path="/rm-login" element={<LoginRm />} />
        <Route path="/detail-consultant" element={<DetailConsultant />} />
        <Route path="/product-recomended/" element={<ProductRecom />} />
        <Route path="/form-consul" element={<FormConsul />} />
        <Route path="/rm" element={<DashboardRm />}>
          <Route index element={<RmDashboard />} />
          <Route path="consumers" element={<ConsumerRm />} />
          <Route path="list-products" element={<RmListProduct />} />
        </Route>
        <Route path="/profil-page" element={<ProfilPage />} />
      </Routes>
      <ToastContainer position="top-center" />
    </BrowserRouter>
  );
}

export default App;
