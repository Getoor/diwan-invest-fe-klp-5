import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { ProductCard } from "../../components";
import NavRm from "../../components/nav-rm";
import api from "../../service/api";
const RmListProduct = () => {
  const location = useLocation();
  const [endUrl, setEndUrl] = useState("");
  const [listProduct, setListProduct] = useState([]);
  const { rm } = useSelector((store) => store.rm);

  const getEndPoint = () => {
    if (location.state.categoryproduct === "agresif") {
      setEndUrl("all");
    } else if (location.state.categoryproduct === "moderat") {
      setEndUrl("konsandmoderat");
    } else if (location.state.categoryproduct === "konservatif") {
      setEndUrl("bytype/konservatif");
    }
  };

  const fetchProduct = async () => {
    try {
      const url = `/api/v1/products/${endUrl}`;
      const response = await api.get(url);
      const payload = [...response.data.data];
      setListProduct(payload);
    } catch (error) {}
  };

  useEffect(() => {
    getEndPoint();
    fetchProduct();
    console.log(endUrl);
    //console.log(location.state.categoryproduct);
  }, [endUrl]);

  return (
    <div className="bg-slate-500">
      <NavRm name={rm?.name} />
      <div className="w-full min-h-screen p-4 mt-3">
        <div>
          <h1 className="font-[poppins] text-2xl text-white font-extrabold">List Product {location.state.categoryproduct}</h1>
          <div className="grid grid-cols-4 gap-5 ">
            {listProduct.map((item) => {
              return <ProductCard firstletter={item.productname.charAt(0)} productname={item.productname} product={item.product} desc={`${item.productdesc.substring(0, 70)} .....`} url={item.producturl} />;
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default RmListProduct;
