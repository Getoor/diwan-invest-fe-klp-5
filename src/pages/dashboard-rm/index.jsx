import React from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import NavRm from "../../components/nav-rm";

const RmDashboard = () => {
  const { rm } = useSelector((store) => store.rm);

  const navigate = useNavigate();

  useEffect(() => {
    if (!rm) {
      navigate("/rm-login");
    }
  }, []);

  return (
    <div className="bg-slate-500 h-full">
      <NavRm name={rm?.name} />

      <div className="w-full">
        <div className="shadow-lg hover:bg-slate-300 rounded-lg bg-slate-200 m-5">
          <div className="p-3 flex flex-col">
            <p className="text-2xl text-center font-extrabold mt-3">DATA CONSUMER</p>
            <p className="text-slate-600 text-lg text-center font-semibold">{rm?.users?.length}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RmDashboard;
