import React from 'react'
import dataJson from '../../data/datapertanyaan.json'
import ImgQuiz from '../../assets/img/questions.png'
import { useState, useEffect  } from 'react'
import { HashLoader } from "react-spinners";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Quiz = () => {
  const [quiz, setQuiz] = useState([]);
  const [question, setQuestion] = useState(0);
  const [score, setScore] = useState(0);
  const [maxQuestions, setMaxQuestions] = useState(15);
  const [loading, setLoading] = useState();
  const [recomended, setRecomended] = useState("");

  const { user } = useSelector((store) => store.user);

  const quizData = async () => {
    setQuiz(dataJson.diwan[0].pertanyaans);
  };

  const updateScore = (answer_index, el) => {
    if (question >= 14) {
      if (score >= 90) {
        setRecomended("agresif");
      } else if (score >= 70 && score < 90) {
        setRecomended("moderat");
      } else if (score < 70) {
        setRecomended("konservatif");
      }
    }
    setScore((current_score) => current_score + quiz[question].benar[answer_index]);

    setTimeout(() => {
      setQuestion((current_question) => current_question + 1);
    }, 100);
  };

  useEffect(() => {
    if (!user) {
      window.location.href = "http://localhost:3000/";
    }
    quizData();
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 4000);
  }, [recomended]);

  return (
    <div>
      {loading ? (
        <div className="grid h-screen place-items-center">
          <div>
            <HashLoader className="m-auto" color="#FDDA1C" size={120} />
            <p className="text-2xl text-center mt-7 text-slate-600 ont-extrabold ml-5 font-semibold">{`Halo, ${user?.name}`}</p>
            <p className="text-2xl text-center text-yellow-400 font-extrabold ml-5 ">Loading...</p>
          </div>
        </div>
      ) : question < maxQuestions ? (
        <div>
          <div className="container h-screen m-auto flex flex-col">
            <div className="text-center py-4 bg-yellow-200 mt-3 shadow-md rounded-md">
              <p className="font-[poppins] my-auto font-semibold text-2xl text-slate-700">RISK PROFILE ASSESMENT </p>
            </div>
            <div className="items-center justify-center my-auto">
              <img src={ImgQuiz} alt="QuizImage" width={270} className="mx-auto mb-4" />
              <div className="text-lg text-center">
                <p className="text-lg font-[poppins] font-thin text-slate-600">{question + 1} / 15 </p>
              </div>
              <div className="p-2 text-xl text-center">
                <p className="text-2xl font-[poppins] font-bold text-slate-700">{quiz[question]?.pertanyaan}</p>
              </div>
              <div className="p-2">
                {quiz[question]?.jawaban.map((data, index) => (
                  <div className="container bg-yellow-200 items-center flex border hover:bg-slate-200 border-slate-300 mb-2 rounded-lg cursor-pointer" key={index} onClick={(el) => updateScore(index, el)}>
                    <div className="text-center ml-6 text-black font-extrabold text-lg">{index + 1}. </div>
                    <div className="py-2 px-4 text-slate-700 font-bold font-[poppins]">{data}</div>
                  </div>
                ))}
              </div>
            </div>
            <div className="container text-center p-2 m-auto">
              <div className="text-xl font-[poppins] border-b border-slate-500 pb-1"></div>
              <p className="text-lg font-[poppins] text-slate-700 mt-2">Soal di tandai dengan hak cipta perusahaan </p>
            </div>
          </div>
        </div>
      ) : (
        <div className=" bg-yellow-300 h-screen  flex flex-wrap items-center justify-center">
          <div className="container max-w-lg bg-white rounded  shadow-lg transform duration-200 easy-in-out m-12">
            <div className="">
              <div className="px-7 mb-8">
                <h2 className="text-xl font-bold text-slate-800 mt-5">Yeahh... Anda telah mengambil Quostioner</h2>
                <p className=" text-sm  font-semibold mt-2">Lihat Product yang di rekomendasikan sekarang</p>
                <Link
                  to="/product-recomended"
                  state={{
                    recomended: recomended,
                    score: score,
                  }}
                >
                  <div className="dark:bg-slate-600 w-40 p-3 mt-3  text-center text-white font-medium text-base rounded-lg hover:bg-slate-700 cursor-pointer  shadow-lg">Lihat</div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Quiz
