import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { putScoreUser } from "../../utils/user/UserSlice";

const FormConsul = () => {
  const location = useLocation();
  const [date, setDate] = useState();
  const dispatch = useDispatch();
  const { isLoading, user } = useSelector((store) => store.user);

  const [userData, setUserData] = useState({
    userid: user?.userid || "",
    scorequis: location.state.score || "",
    dateconsult: date || "",
    productrecom: location.state.productrecom || "",
    consultanid: location.state.consultantid || "",
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(putScoreUser(userData));
  };

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setUserData({ ...userData, [name]: value });
  };

  useEffect(() => {
    if (!user) {
      window.location.href = "http://localhost:3000/";
    }
  }, []);
  return (
    <div>
      <div className="flex justify-center items-center h-screen bg-yellow-100">
        <form onSubmit={handleSubmit}>
          <div className="p-6 shadow-lg bg-slate-100 rounded-md">
            <h1 className="text-slate-700 font-[poppins] text-2xl text-center font-extrabold">Berkonsultasi dengan {location.state.consultanname}</h1>
            <h1 className="text-slate-500 mt-3 font-[poppins] text-xl text-center font-semibold">Berpengalaman selama {location.state.experience}</h1>
            <hr className="mt-3" />
            <p className="text-slate-500 mt-3 font-[poppins] text-lg  ">Pilih Jadwal Konsultasi anda</p>
            <input type="hidden" name="userid" value={userData.userid} onChange={handleChange} />
            <input type="hidden" name="scorequis" value={userData.scorequis} onChange={handleChange} />
            <input type="hidden" name="consultantid" value={userData.consultanid} onChange={handleChange} />
            <input className="bg-red-200 w-36 h-12 p-3 rounded-md cursor-pointer" type="date" name="dateconsult" value={userData.dateconsult} onChange={handleChange} />
            <p className="text-red-300 mt-3 font-[poppins] text-lg  ">*konsultasi hanya di lakukan saat hari kerja, pastikan pilih sesuai hari kerja</p>
            <p className="text-slate-500 mt-3 font-[poppins] text-lg  ">Anda memilih tanggal : {userData.dateconsult}</p>
            <button type="submit" className="bg-transparent hover:bg-slate-600 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded">
              {isLoading ? "Mengirim..." : "Kirim jadwal"}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default FormConsul;
