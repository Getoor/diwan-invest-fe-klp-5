import { Konsultasi, Contact } from "@components";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { HashLoader } from "react-spinners";

const Konsultan = () => {
  const [loading, setLoading] = useState();
  const { user } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user) {
      window.location.href = "http://localhost:3000/";
    }
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  }, []);

  return (
    <div>
      {loading ? (
        <div className="grid h-screen place-items-center">
          <div>
            <HashLoader className="m-auto" color="#FDDA1C" size={120} />
            <p className="text-2xl mt-10 text-yellow-400 font-extrabold ml-5 italic">Loading...</p>
          </div>
        </div>
      ) : (
        <div>
          <Konsultasi />
          <Contact />
        </div>
      )}
    </div>
  );
};

export default Konsultan;
