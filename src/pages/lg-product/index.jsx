import React from "react";
import { Header } from "@components";
import ReksaDana from "../../assets/img/reksadana.png";
import Asuransi from "../../assets/img/asuransi.png";

const Product = () => {
  return (
    <div>
      <Header />
      <div>
        {/* reksa dana  */}
        <div className="w-full py-16 px-4">
          <div className="mt-14 max-w-[1100px] mx-auto grid md:grid-cols-2">
            <img src={ReksaDana} alt="Reksa Dana" width={400} height={400} className="shadow-md" />
            <div className="flex flex-col justify-center">
              <p className="font-[poppins] text-yellow-400 font-extrabold text-4xl">Reksa Dana</p>
              <div class="border-b-8 border-yellow-400 w-52 shadow-sm"></div>
              <p className="mt-5 font-semibold text-base font-[poppins]">
                Ketahui secara tepat dan bijak apa yang kamu beli untuk awal hidup yang lebih bermakna. Sesuaikan reksa dana dengan tujuan keuanganmu dan cermati kinerjanya secara detail. Nikmatilah hasil yang maksimal dengan modal
                terjangkau.
              </p>
            </div>
          </div>
        </div>

        <div class="border-t-2 border-yellow-400 w-auto mx-auto ml-20 mr-20 my-10"></div>

        {/* asuransi */}
        <div className="w-full py-16 px-4">
          <div className="mt-5 max-w-[1100px] mx-auto grid md:grid-cols-2">
            <div className="flex flex-col justify-center">
              <p className="font-[poppins] text-yellow-400 font-extrabold text-4xl">Asuransi</p>
              <div class="border-b-8 border-yellow-400 w-52 shadow-sm"></div>
              <p className="mt-5 font-semibold text-base font-[poppins]">Perlindungan ekstra untuk kamu yang pantang menyerah. Kini semakin percaya diri dalam wujudkan mimpi dengan goals keuangan yang terproteksi.</p>
            </div>
            <img src={Asuransi} alt="Reksa Dana" width={400} height={400} className="shadow-md ml-10" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
