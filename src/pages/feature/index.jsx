import React from "react";
import { Header } from "@components";
import Consultation from "../../assets/img/consultasi.png";
import Recommendation from "../../assets/img/recommendation.png";
import Quesioner from "../../assets/img/quesioner.png";
const Feature = () => {
  return (
    <div>
      <Header />
        {/* quesioner  */}
        <div className="w-full py-16 px-4">
          <div className="mt-14 max-w-[1100px] mx-auto grid md:grid-cols-2">
            <img src={Quesioner} alt="Quesioner" width={400} height={400} className="shadow-md" />
            <div className="flex flex-col justify-center">
              <p className="font-[poppins] text-yellow-400 font-extrabold text-4xl">Quesioner</p>
              <div class="border-b-8 border-yellow-400 w-52 shadow-sm"></div>
              <p className="mt-5 font-semibold text-base font-[poppins]">
               Bingung mau berinvestasi? Ayo isi quesionermu untuk menentukan dimana kamu akan berinvestasi. Karena jawabanmu menentukan investasimu.
              </p>
            </div>
          </div>
        </div>
      <div class="border-t-2 border-yellow-400 w-auto mx-auto ml-20 mr-20 my-10"></div>

      {/* consultation */}
     <div className="w-full py-16 px-4">
          <div className="mt-14 max-w-[1100px] mx-auto grid md:grid-cols-2">
            <img src={Consultation} alt="Consultation" width={400} height={400} className="shadow-md" />
            <div className="flex flex-col justify-center">
              <p className="font-[poppins] text-yellow-400 font-extrabold text-4xl">Consultation</p>
              <div class="border-b-8 border-yellow-400 w-52 shadow-sm"></div>
              <p className="mt-5 font-semibold text-base font-[poppins]">
                Curhat keuangan bareng Certified Financial Planner yang siap beri arahan tanpa paksaan. Jadikan sarannya sebagai asupan tambahan dalam perjalananmu
                membangun kekayaan.
              </p>
            </div>
          </div>
        </div>
    <div class="border-t-2 border-yellow-400 w-auto mx-auto ml-20 mr-20 my-10"></div>

    {/* recommendation  */}
  <div className="w-full py-16 px-4">
      <div className="mt-14 max-w-[1100px] mx-auto grid md:grid-cols-2">
        <img src={Recommendation} alt="Recommendation" width={400} height={400} className="shadow-md" />
          <div className="flex flex-col justify-center">
            <p className="font-[poppins] text-yellow-400 font-extrabold text-4xl">Recomendation</p>
              <div class="border-b-8 border-yellow-400 w-72 shadow-sm"></div>
                <p className="mt-5 font-semibold text-base font-[poppins]">
                  Pilihlah investasimu sesuai dari rekomendasi para ahli.
               </p>
             </div>
            </div>
          </div>
        </div>
      
    );
  };

export default Feature;
