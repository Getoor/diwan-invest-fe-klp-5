import React, { useEffect, useState } from "react";
import LoginImg from "../../assets/img/login.png";
import { Input, Space } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import { BsArrowLeftCircle } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { loginUser } from "../../utils/user/UserSlice";

const initialState = {
  email: "",
  password: "",
};

const Login = () => {
  const [values, setValues] = useState(initialState);

  const { user, isLoading } = useSelector((store) => store.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setValues({ ...values, [name]: value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const { email, password } = values;
    dispatch(loginUser({ email, password }));
  };

  useEffect(() => {
    if (user) {
      setTimeout(() => {
        navigate("/home");
      });
    }
  }, [user, navigate]);

  return (
    <div>
      <div className="grid grid-cols-1 sm:grid-cols-2 h-screen w-full">
        <div className="hidden sm:block">
          <img className="w-full h-full object-cover " src={LoginImg} alt="Login Photo" />
        </div>
        <div className="flex flex-col justify-center ">
          <form className="max-w-[400px] w-full mx-auto p-8 px-8 rounded-lg bg-gray-100" onSubmit={onSubmit}>
            <h2 className="text-5xl text-black font-[poppins] font-bold text-center">
              Sign In <br />
            </h2>
            <h2 className="text-4xl text-[#FDDA1C] font-[poppins] font-semibold text-center">Diwan Invest</h2>
            <div className="flex flex-col py-1 font-[poppins] text-black">
              <label className="text-black mb-2" htmlFor="email">
                Email
              </label>
              <Space direction="vertical">
                <Input
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  id="email"
                  type="email"
                  className="rounded-lg mt-2 p-2 bg-white focus:outline-none invalid:text-red-700 invalid:focus:ring-red-700 invalid:focus:border-red-700"
                  placeholder="Email"
                />
              </Space>
              {/* <p className="text-sm m-0.5 text-red-700  peer-invalid:visible invisible ">Email not valid</p> */}
            </div>
            <div className="flex flex-col py-1 font-[poppins] text-black">
              <label className="text-black" htmlFor="password">
                Password
              </label>
              <Space direction="vertical">
                <Input.Password
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  className="rounded-lg mt-3 p-2 bg-white focus:outline-none"
                  placeholder="Password"
                  iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                />
              </Space>
            </div>

            <button disabled={isLoading} type="submit" className="mt-5 w-full my-1 mb-6 py-4 bg-[#FDDA1C] font-[poppins] text-black  font-medium text-lg px-4 border  rounded">
              {isLoading ? `Loading...` : `Login`}
            </button>

            <div className="  font-[poppins] text-black">
              <p className="text-center">
                not have account ?{" "}
                <span className="cursor-pointer font-semibold text-yellow-500">
                  <Link to={"/register"}>SIGN UP HERE</Link>
                </span>
              </p>
            </div>

            <div>
              <Link to={"/"}>
                <BsArrowLeftCircle className="w-8 h-8 m-auto mt-5 text-yellow-500" />
              </Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
