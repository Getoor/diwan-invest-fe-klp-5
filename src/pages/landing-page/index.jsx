import { Header } from "@components";
import GoogleImg from "../../assets/img/google.png";
import AppImg from "../../assets/img/appstore4.png";
import OjkImg from "../../assets/img/ojk.png";
import KominfoImg from "../../assets/img/kominfo.png";
import Home from "../../assets/img/landingpage.png";

const LandingPage = () => {
  return (
    <div>
      <Header />
      <div>
        <div className="w-screen">
          <div className="flex mt-10  w-screen  justify-center p-10">
            <div className="ml-10 md:ml-10 mt-20 ">
              <h2 className="my-5 text-5xl font-bold ">Safe and Convenient Investment for Beginner</h2>
              <p className="text-lg">Libatkan dirimu dalam perkembangan seluruh produk investasimu secara terperinci, dimana saja dan kapan saja.</p>

              <div className="mt-12 flex items-start justify-start gap-5 ">
                <img src={GoogleImg} alt="" />
                <img src={AppImg} alt="" />
              </div>
              <div className="mt-5 flex items-start justify-start gap-5 ">
                <img src={OjkImg} alt="" />
                <img src={KominfoImg} alt="" />
              </div>
            </div>
            <div className=" mr-5 hidden md:block">
              <img src={Home} alt="Home" width={900} height={700} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
