import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginRm } from "../../utils/rm/ConsultanSlice";

const initialState = {
  email: "",
  password: "",
};

const LoginRm = () => {
  const [values, setValues] = useState(initialState);

  const { rm, isLoading } = useSelector((store) => store.rm);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setValues({ ...values, [name]: value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const { email, password } = values;
    dispatch(loginRm({ email, password }));
  };

  useEffect(() => {
    if (rm) {
      setTimeout(() => {
        navigate("/rm");
      }, 1000);
    }
  }, [rm, navigate]);
  return (
    <div>
      <div className="flex justify-center items-center h-screen bg-yellow-200">
        <form onSubmit={onSubmit}>
          <div className="w-96 p-6 shadow-lg bg-slate-100 rounded-md">
            <h1 className="text-slate-700 font-[poppins] text-2xl text-center font-extrabold">RM LOGIN</h1>
            <hr className="mt-3" />
            <div className="mt-3">
              <label htmlFor="email" className="block text-base mb-2">
                Email
              </label>
              <input
                name="email"
                value={values.email}
                onChange={handleChange}
                id="email"
                type="email"
                className="invalid:text-red-700 invalid:focus:ring-red-700 invalid:focus:border-red-700 border w-full text-base px-2 py-1 focus:outline-none focus:ring-0 focus:border-slate-500 "
                placeholder="email"
              />
            </div>
            <div className="mt-3">
              <label htmlFor="password" className="block text-base mb-2">
                Password
              </label>
              <input
                name="password"
                value={values.password}
                onChange={handleChange}
                type="password"
                id="password"
                className="border w-full text-base px-2 py-1 focus:outline-none focus:ring-0 focus:border-slate-500 "
                placeholder="password"
              />
            </div>
            <div className="mt-5">
              <button type="submit" className="border-2 rounded-md bg-yellow-400 text-slate-700 font-bold py-2 w-full">
                {isLoading ? `Loading...` : `Login`}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginRm;
