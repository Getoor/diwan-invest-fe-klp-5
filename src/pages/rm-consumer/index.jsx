import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import NavRm from "../../components/nav-rm";

const ConsumerRm = () => {
  const { rm } = useSelector((store) => store.rm);

  return (
    <div className="bg-slate-500">
      <NavRm name={rm?.name} />

      <div className="w-full min-h-screen p-4 mt-3">
        {rm?.users.length === 0 ? (
          <div>
            <h1 className="font-[poppins] text-2xl text-white font-extrabold">Belum ada Consumer</h1>
          </div>
        ) : (
          <div>
            <h1 className="font-[poppins] text-2xl text-white font-extrabold">Consumer Anda</h1>
            <table className="w-full mt-5 shadow-lg">
              <thead className="bg-yellow-200 border-b-2 border-gray-200">
                <tr>
                  <th className="p-3 text-sm font-semibold tracking-wide text-left">Nama</th>
                  <th className="p-3 text-sm font-semibold tracking-wide text-left">Email</th>
                  <th className="p-3 text-sm font-semibold tracking-wide text-left">Category Product</th>
                  <th className="p-3 text-sm font-semibold tracking-wide text-left">Risk Profile</th>
                  <th className="p-3 text-sm font-semibold tracking-wide text-left">Date Consult</th>
                  <th className="p-3 text-sm font-semibold tracking-wide text-left">Hubungi Consumer</th>
                  <th className="p-3 text-sm font-semibold tracking-wide text-left">Start Journey</th>
                </tr>
              </thead>
              <tbody className="bg-slate-200 text-base font-medium">
                {rm.users.map((item) => {
                  return (
                    <tr>
                      <td className="p-3 ">{item?.name}</td>
                      <td className="p-3 ">{item?.email}</td>
                      <td className="p-3 ">{item?.productrecom}</td>
                      <td className="p-3 ">{item?.score_quistioner}</td>

                      <td className="p-3 ">{item?.date_consult}</td>
                      <td className="p-3 text-center">
                        <a href={`https://api.whatsapp.com/send?phone=${item?.phonenumber}`} target="_blank" className="text-xs font-medium uppercase tracking-wider text-yellow-200 bg-green-500 p-2 rounded-md cursor-pointer shadow-md">
                          hubungi
                        </a>
                      </td>
                      <td className="p-3 text-center">
                        <NavLink
                          to="/rm/list-products"
                          state={{
                            categoryproduct: item?.productrecom,
                          }}
                        >
                          <span className=" text-xs font-medium uppercase tracking-wider text-yellow-200 bg-blue-400 p-2 rounded-md cursor-pointer shadow-md">Start</span>
                        </NavLink>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        )}
      </div>
    </div>
  );
};

export default ConsumerRm;
