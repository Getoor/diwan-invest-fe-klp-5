import { Rate } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { HashLoader } from "react-spinners";
import Contact from "../../components/contact";
import api from "../../service/api";

const ProductRecom = () => {
  const [consultan, setConsultan] = useState([]);
  const [loading, setLoading] = useState();
  const { user } = useSelector((store) => store.user);
  const [endPoint, setEndPoint] = useState("");
  const [product, setProduct] = useState([]);
  const location = useLocation();

  const getEndPoint = () => {
    if (location.state.recomended === "agresif") {
      setEndPoint("all");
    } else if (location.state.recomended === "moderat") {
      setEndPoint("konsandmoderat");
    } else if (location.state.recomended === "konservatif") {
      setEndPoint("bytype/konservatif");
    }
  };

  const fetchProduct = async () => {
    try {
      const url = `/api/v1/products/${endPoint}`;
      const response = await api.get(url);
      const payload = [...response.data?.data];
      setProduct(payload);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getEndPoint();

    fetchProduct();
  }, [endPoint]);

  const fetchConsultant = async () => {
    try {
      const url = "/api/v1/consultant";
      const response = await api.get(url);
      const payload = [...response.data];
      setConsultan(payload);
    } catch (error) {
      //toast.error(error.response.data.data);
      console.log(error);
    }
  };

  useEffect(() => {
    if (!user) {
      window.location.href = "http://localhost:3000/";
    }

    fetchConsultant();
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, []);

  return (
    <div>
      {loading ? (
        <div className="grid h-screen place-items-center">
          <div>
            <HashLoader className="m-auto" color="#FDDA1C" size={120} />
            <p className="text-2xl text-center mt-7 text-slate-600 ont-extrabold ml-5 font-semibold"></p>
            <p className="text-2xl text-center text-yellow-400 font-extrabold ml-5 ">Loading...</p>
          </div>
        </div>
      ) : (
        <div>
          <div className="grid m-auto place-items-center bg-yellow-400 ">
            <p className="text-2xl text-center mt-7 text-slate-700  ml-5 font-semibold">{`Halo, ${user?.name}`}</p>
            <p className="text-2xl text-slate-700  ml-5 font-medium">Product yang di rekomendasikan : {location.state.recomended}</p>
          </div>
          <div>
            <div className="mx-6 py-10">
              <h1 className="text-center text-2xl font-[poppins] font-bold"> Konsultasikan Sekarang </h1>

              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 mt-5">
                {consultan.map((items) => {
                  return (
                    <div key={items.id} className="shadow-lg hover:bg-slate-300 rounded-lg bg-slate-200 m-3">
                      <div className="p-2 flex flex-col">
                        <div className="rounded-full mx-auto overflow-hidden w-36 h-36 bg-slate-400 ">
                          <p className="text-slate-700 font-[poppins] mt-10 text-center text-6xl font-extrabold">{items.name.charAt(0)}</p>
                        </div>
                        <div className="mx-auto mt-2">
                          <Rate className="h-5 " defaultValue={items.rate} />
                        </div>
                        <p className="text-2xl text-center font-semibold mt-2">{items.name}</p>
                        <p className="text-xl text-center font-medium ">Experience Selama {items.experience}</p>
                        <p className="text-slate-600 text-lg text-justify">{items.description.substring(0, 70)} ....</p>

                        <div className="grid grid-cols-2 m-auto ">
                          <Link
                            to="/detail-consultant"
                            state={{
                              consultantid: items.consultantid,
                            }}
                          >
                            <div className="bg-yellow-400 w-40 p-3 text-black text-center font-medium text-base rounded-lg hover:bg-yellow-500 cursor-pointer shadow-lg">Detail</div>
                          </Link>
                          <Link
                            to="/form-consul"
                            state={{
                              consultantid: items.consultantid,
                              consultanname: items.name,
                              experience: items.experience,
                              score: location.state.score,
                              productrecom: location.state.recomended,
                            }}
                          >
                            <div className="dark:bg-slate-600 w-40 p-3 ml-3 text-center text-white font-medium text-base rounded-lg hover:bg-slate-700 cursor-pointer shadow-lg">Konsultasi</div>
                          </Link>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className="mx-6 py-10">
                <h1 className="text-center text-2xl font-[poppins] font-bold"> Product Yang di Rekomendasikan </h1>
                <div className="mt-2">
                  <table className="w-full mt-2 shadow-lg">
                    <thead className="bg-yellow-200 border-b-2 border-gray-200">
                      <tr>
                        <th className="p-3 text-sm font-semibold tracking-wide text-left">Level Product</th>
                        <th className="p-3 text-sm font-semibold tracking-wide text-left">Product</th>

                        <th className="p-3 text-sm font-semibold tracking-wide text-left">Nama Product</th>
                        <th className="p-3 text-sm font-semibold tracking-wide text-left">Product Desc</th>

                        {/* <th className="p-3 text-sm font-semibold tracking-wide text-left">Date Consult</th> */}
                      </tr>
                    </thead>
                    <tbody className="bg-slate-200 text-base font-medium">
                      {product?.map((items) => {
                        return (
                          <tr key={items?.productid}>
                            <td className="p-3 bg-slate-300 text-purple-600">{items?.productType.toUpperCase()}</td>
                            <td className="p-3 text-red-700 font-extrabold">{items?.product}</td>

                            <td className="p-3 text-red-700 font-extrabold">{items?.productname.toUpperCase()}</td>
                            <td className="p-3 ">{items?.productdesc}</td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="w-full">
                <Contact />
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ProductRecom;
