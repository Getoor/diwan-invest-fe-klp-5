export {default as LandingPage} from './landing-page'
export {default as Login} from './login'
export {default as Register} from './register'
export {default as Home} from './home'
export {default as Quiz} from './question'
export { default as ProfilPage } from "./profil-page";
export { default as About } from "./about";
export { default as Feature } from "./feature";
export { default as Product } from "./lg-product";
export { default as ErrorPage } from "./eror";
export { default as Konsultan } from "./konsultansi";
export { default as LoginRm } from "./login-rm";
export { default as DashboardRm } from "./rm";
export { default as RmDashboard } from "./dashboard-rm";
export { default as DetailConsultant } from "./detail-consultant";
export { default as ProductRecom } from "./product-recom";
export { default as FormConsul } from "./form-konsul";
export { default as ConsumerRm } from "./rm-consumer";
export { default as RmListProduct } from "./rm-products";

