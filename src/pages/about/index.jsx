import React from "react";
import { Header } from "@components";
import DiwanInvest from "../../assets/img/logo.png";
const About = () => {
  return (
    <div>
      <Header />
      <div className="w-full py-16 px-4">
        <div className="mt-14 max-w-[1100px] mx-auto grid md:grid-cols-2">
          <img src={DiwanInvest} alt="DiwanInvest" width={400} height={400} className="shadow-md" />
          <div className="flex flex-col justify-center">
            <p className="font-[poppins] text-yellow-400 font-extrabold text-4xl">DiwanInvest</p>
            <div class="border-b-8 border-yellow-400 w-52 shadow-sm"></div>
            <p className="mt-5 font-semibold text-base font-[poppins]">
              DiwanInvest adalah aplikasi investasi untuk para investor pasif menumbuhkan kekayaann secara berkelanjutan, aman dan mudah. Dalam aplikasi ini tersedia berbagai instrumen investasi seperti reksa dana dan emas yanng dikurasi
              secara khusus untuk mencapai tujuan keuangan anda.
            </p>
          </div>
        </div>
        <div class="border-t-2 border-yellow-400 w-auto mx-auto ml-20 mr-20 my-10"></div>
      </div>
    </div>
  );
};

export default About;
