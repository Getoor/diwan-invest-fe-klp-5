import { Button } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

const ProfilPage = () => {
  const { user } = useSelector((store) => store.user);

  return (
    <div class="h-screen bg-yellow-300 flex flex-wrap items-center  justify-center mx-auto">
      <div class="container lg:w-2/5 xl:w-2/7 sm:w-full md:w-2/3 w-full px-4 mx-auto bg-gray-100  shadow-lg    transform   duration-200 easy-in-out">
        <NavLink to={"/home"}>
          <Button className=" mt-2 border-2 py-2 rounded-lg border-b-4 font-black translate-y-2 border-l-4">Home</Button>
        </NavLink>
        <div class=" px-6 overflow-hidden"></div>
        <div className="rounded-full mx-auto overflow-hidden w-36 h-36 bg-yellow-300">
          <p className="text-slate-700 font-[poppins] mt-10 text-center text-6xl font-extrabold">{user?.name.charAt(0)}</p>
        </div>
        <div class="my-4 flex flex-col 2xl:flex-row space-y-4 2xl:space-y-0 2xl:space-x-4">
          <div class="w-full flex flex-col 2xl:w-1/3">
            <div class="flex-1 bg-white rounded-lg shadow-xl p-8">
              <h4 class="text-xl text-gray-900 font-bold">Personal Info</h4>
              <ul class="mt-2 text-gray-700">
                <li class="flex border-y py-2">
                  <span class="font-bold w-24">Nama:</span>
                  <span class="text-gray-700">{user?.name}</span>
                </li>
                {/* <li class="flex border-b py-2">
                  <span class="font-bold w-24">TTL:</span>
                  <span class="text-gray-700">20 Jul, 1991</span>
                </li> */}
                <li class="flex border-b py-2">
                  <span class="font-bold w-24">Joined:</span>
                  <span class="text-gray-700">{user?.create_at}</span>
                </li>
                <li class="flex border-b py-2">
                  <span class="font-bold w-24">No.Hp:</span>
                  <span class="text-gray-700">{user?.phonenumber}</span>
                </li>
                <li class="flex border-b py-2">
                  <span class="font-bold w-24">Email:</span>
                  <span class="text-gray-700">{user?.email}</span>
                </li>
                <li class="flex border-b py-2">
                  <span class="font-bold w-24">Lokasi:</span>
                  <span class="text-gray-700">{user?.address}</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfilPage;
