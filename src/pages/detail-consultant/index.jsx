import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { BounceLoader } from "react-spinners";
import api from "../../service/api";

const DetailConsultant = () => {
  const [consultant, setConsultan] = useState([]);
  const location = useLocation();
  const [loading, setLoading] = useState();
  const { user } = useSelector((store) => store.user);

  const getConsultant = async () => {
    try {
      const url = `/api/v1/consultant/byid/${location.state.consultantid}`;
      const response = await api.get(url);

      const payload = [response?.data?.data];

      setConsultan(payload[0]);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    //console.log(location.state.consultantid);
    //console.log(consultant?.products.length);
    if (!user) {
      window.location.href = "http://localhost:3000/";
    }
    getConsultant();
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  return (
    <div>
      {loading ? (
        <div className="grid h-screen place-items-center">
          <div>
            <BounceLoader className="m-auto" color="#FDDA1C" size={120} />
          </div>
        </div>
      ) : (
        <div className=" bg-slate-200 h-screen  flex flex-wrap items-center justify-center">
          <div className="container max-w-lg bg-white rounded  shadow-lg transform duration-200 easy-in-out m-12">
            <div className="rounded-full mx-auto overflow-hidden mt-5 w-36 h-36 bg-slate-400 ">
              <p className="text-slate-700 font-[poppins] mt-10 text-center text-6xl font-extrabold">{consultant?.name?.charAt(0)}</p>
            </div>
            <div className="">
              <div className="px-7 mb-8">
                <h2 className="text-3xl font-bold text-slate-800 mt-5">{consultant?.name}</h2>
                <div className=" px-4 py-2 bg-yellow-200 rounded-lg text-sm font-semibold">{consultant?.address}</div>
                <div className=" px-4 py-2 bg-yellow-200 mt-2 rounded-lg text-sm font-semibold">Experience Selama {consultant?.experience}</div>
                <p className="text-slate-600 text-lg text-justify mt-2">{consultant?.description}</p>
                <div className="bg-yellow-400 w-40 p-3 text-center font-medium text-base rounded-lg hover:bg-yellow-500  shadow-lg">{consultant?.users?.length} Consumer</div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default DetailConsultant;
